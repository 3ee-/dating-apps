function getSize(size) {
  let sizeNumber = 0;
  switch (size) {
    case 'small':
      sizeNumber = 30;
      break;
    case 'medium':
      sizeNumber = 40;
      break;
    case 'big':
      sizeNumber = 50;
      break;
    case 'huge':
      sizeNumber = 60;
      break;
    default:
      sizeNumber = 50;
  }
  return sizeNumber;
}

export { getSize };
