const flexRow = {
  flexDirection: 'row',
};

const spaceBetween = {
  justifyContent: 'space-between',
};

const spaceEvenly = {
  justifyContent: 'space-evenly',
};

export { flexRow, spaceBetween, spaceEvenly };
