import color from './color';

const small = 12;
const medium = 16;
const big = 20;
const huge = 24;

const fontSizes = {
  fontSmall: {
    fontSize: small,
    fontWeight: '300',
    color: color.black,
    fontFamily: 'Mulish-Regular',
  },
  fontMedium: {
    fontSize: medium,
    fontWeight: '300',
    color: color.black,
    fontFamily: 'Mulish-Regular',
  },
  fontBig: {
    fontSize: big,
    fontWeight: '300',
    color: color.black,
    fontFamily: 'Mulish-Regular',
  },
  fontHuge: {
    fontSize: huge,
    fontWeight: '300',
    color: color.black,
    fontFamily: 'Mulish-Regular',
  },
};

export default fontSizes;
