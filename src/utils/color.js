const color = {
  primaryColor: '#FF4A73',
  secondaryColor: '#FF9DB3',
  gray: '#959595',
  white: '#ffffff',
  yellow: '#FEC537',
  black: '#4A4A4A',
  smokeGray: '#DDDDDD',
};

export default color;
