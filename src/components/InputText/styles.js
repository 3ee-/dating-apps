import { StyleSheet } from 'react-native';

// Utilities
import fontSizes from '@utils/fontSizes';
import color from '@utils/color';
import { deviceWidth } from '@utils/device';

const styles = StyleSheet.create({
  textInputContainer: {
    backgroundColor: color.white,
    marginBottom: 20,
  },
  textInputLabel: {
    ...fontSizes.fontSmall,
    color: color.gray,
    fontWeight: '600',
  },
  textInput: {
    minHeight: 40,
    ...fontSizes.fontMedium,
    color: color.black,
    fontWeight: '600',
    borderBottomColor: color.smokeGray,
    borderBottomWidth: 1,
  },
});

export default styles;
