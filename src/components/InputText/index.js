import React from 'react';
import { Text, View, TextInput } from 'react-native';

// Styles
import styles from './styles';

function InputText(props) {
  const { label, value, onChange, type } = props;
  return (
    <View style={styles.textInputContainer}>
      <Text style={styles.textInputLabel}>{label}</Text>
      <TextInput
        style={styles.textInput}
        value={value}
        onChangeText={onChange}
        secureTextEntry={type === 'password'}
      />
    </View>
  );
}

export default InputText;
