import { StyleSheet } from 'react-native';

// Utilities
import fontSizes from '@utils/fontSizes';
import color from '@utils/color';
import { deviceWidth } from '@utils/device';

const styles = StyleSheet.create({
  container: {
    height: 60,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: color.primaryColor,
    borderRadius: 30,
  },
  textContainer: {
    ...fontSizes.fontMedium,
    color: color.white,
    textAlign: 'right',
    fontWeight: '600',
  },
  roundedContainer: {
    width: 60,
    height: 60,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 30,
  },
});

export default styles;
