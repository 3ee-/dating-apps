import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import { getSize } from '../../utils/size';

// Styles
import styles from './styles';

function Button(props) {
  const {
    style,
    text,
    size,
    color,
    textStyle,
    onPress,
    type,
    children,
  } = props;
  if (type === 'rounded') {
    return (
      <TouchableOpacity
        onPress={onPress}
        style={[styles.roundedContainer, style]}>
        {children}
      </TouchableOpacity>
    );
  }
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[
        styles.container,
        style,
        {
          height: getSize(size),
        },
      ]}>
      <Text style={[styles.textContainer, textStyle]}>{text}</Text>
    </TouchableOpacity>
  );
}

export default Button;
