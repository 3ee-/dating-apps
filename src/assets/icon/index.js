import Love from './Love';
import People from './People';
import Profile from './Profile';
import Social from './Social';
import Dinner from './Dinner';
import Google from './Google';
import Instagram from './Instagram';
import Facebook from './Facebook';

export { Love, People, Profile, Social, Dinner, Google, Instagram, Facebook };
