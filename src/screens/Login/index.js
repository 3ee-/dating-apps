import React, { useState } from 'react';
import { Image, ScrollView, Text, TouchableOpacity, View } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useNavigation } from '@react-navigation/native';

// Assets
import { Dinner, Facebook, Google, Instagram } from '@assets/icon';
import { backgroundLogin } from '@assets/images';

// Components
import { InputText, Button } from '@components';

// Utils
import { flexRow, spaceEvenly } from '@utils/globalStyles';

// Styles
import styles from './styles';

function Login() {
  const navigation = useNavigation();

  const [email, setEmail] = useState();
  const [password, setPassword] = useState();
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView contentContainerStyle={styles.container}>
        <View style={styles.header}>
          <Image source={backgroundLogin} style={styles.backgroundLogin} />
          <View style={styles.illustration}>
            <Dinner height={200} />
          </View>
        </View>
        <View style={styles.content}>
          <InputText
            label="Email Address / Phone number"
            value={email}
            onChange={(value) => setEmail(value)}
          />
          <InputText
            label="Password"
            value={password}
            onChange={(value) => setPassword(value)}
            type="password"
          />
          <View style={styles.forgotContainer}>
            <TouchableOpacity>
              <Text style={styles.forgotText}>Forgot Password</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.footer}>
          <Button text="Login" onPress={() => navigation.navigate('Home')} />

          <View style={[flexRow, styles.socialWrapper, spaceEvenly]}>
            <Button style={styles.roundedButton} type="rounded">
              <Facebook width={40} height={40} />
            </Button>
            <Button style={styles.roundedButton} type="rounded">
              <Google width={40} height={40} />
            </Button>
            <Button style={styles.roundedButton} type="rounded">
              <Instagram width={40} height={40} />
            </Button>
          </View>

          <TouchableOpacity>
            <Text style={styles.textSignup}>
              New User? <Text style={styles.textSignupRed}>Register</Text>
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

export default Login;
