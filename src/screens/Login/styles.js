import { StyleSheet } from 'react-native';

// Utilities
import fontSizes from '@utils/fontSizes';
import color from '@utils/color';
import { deviceWidth } from '@utils/device';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.white,
    alignItems: 'center',
  },
  header: {
    width: deviceWidth,
    position: 'relative',
    alignItems: 'center',
  },
  backgroundLogin: {
    width: deviceWidth,
    position: 'absolute',
    minHeight: 400,
  },
  illustration: {
    height: 300,
    alignSelf: 'center',
    justifyContent: 'center',
  },
  content: {
    width: '90%',
    marginHorizontal: 30,
    backgroundColor: color.white,
    paddingVertical: 25,
    paddingHorizontal: 20,
  },
  forgotContainer: {
    marginBottom: 20,
  },
  forgotText: {
    ...fontSizes.fontMedium,
    color: color.primaryColor,
    fontWeight: '600',
    textAlign: 'right',
    textDecorationLine: 'underline',
    textDecorationStyle: 'solid',
    textDecorationColor: color.primaryColor,
  },
  footer: {
    width: deviceWidth,
    paddingHorizontal: 30,
    marginTop: 20,
  },
  textSignup: {
    ...fontSizes.fontMedium,
    color: color.gray,
    fontWeight: '600',
    textAlign: 'center',
    marginTop: 20,
  },
  textSignupRed: {
    color: color.primaryColor,
  },
  roundedButton: {
    backgroundColor: color.white,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  socialWrapper: {
    marginVertical: 20,
  },
});

export default styles;
