import { StyleSheet } from 'react-native';

// Utilities
import fontSizes from '@utils/fontSizes';

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    marginHorizontal: 30,
    alignItems: 'center',
  },
  contentText: {
    ...fontSizes.fontMedium,
    marginTop: 50,
    textAlign: 'center',
  },
});

export default styles;
