import React from 'react';
import { View, Text } from 'react-native';

// Styles
import styles from './styles';

function OnBoardingItem({ image, content }) {
  return (
    <View style={styles.container}>
      {image}
      <Text style={styles.contentText}>{content}</Text>
    </View>
  );
}

export default OnBoardingItem;
