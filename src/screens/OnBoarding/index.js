import React, { useState } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { useNavigation } from '@react-navigation/native';

// Assets
import { Love, People, Profile, Social } from '@assets/icon';

// Utils
import { deviceWidth } from '@utils/device';

// Components
import { Button } from '@components';
import OnBoardingItem from './OnBoardingItem';

// Styles
import styles from './styles';

function OnBoarding() {
  const navigation = useNavigation();
  const [entries] = useState([
    {
      image: <Love height={300} />,
      content:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molest.',
    },
    {
      image: <Profile height={300} />,
      content:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molest.',
    },
    {
      image: <People height={300} />,
      content:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molest.',
    },
    {
      image: <Social height={300} />,
      content:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molest.',
    },
  ]);
  const [activeSlide, setActiveSlide] = useState(0);

  const _renderItem = ({ item, index }) => {
    console.log(item);
    return (
      <OnBoardingItem image={item.image} content={item.content} key={index} />
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.skipText}>Skip</Text>
      </View>
      <View style={styles.carousel}>
        <Carousel
          ref={(c) => {
            this._carousel = c;
          }}
          data={entries}
          renderItem={_renderItem}
          sliderWidth={deviceWidth}
          itemWidth={deviceWidth}
          onSnapToItem={(index) => setActiveSlide(index)}
        />
        <Pagination
          dotsLength={entries.length}
          activeDotIndex={activeSlide}
          dotStyle={styles.dot}
          inactiveDotStyle={styles.inactiveDot}
          inactiveDotOpacity={0.4}
          inactiveDotScale={0.6}
        />
      </View>
      <View style={styles.footer}>
        <Button text="Login" onPress={() => navigation.navigate('Login')} />

        <TouchableOpacity>
          <Text style={styles.textSignup}>
            New User? <Text style={styles.textSignupRed}>Register</Text>
          </Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
}

export default OnBoarding;
