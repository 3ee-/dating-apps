import { StyleSheet } from 'react-native';

// Utilities
import fontSizes from '@utils/fontSizes';
import color from '@utils/color';
import { deviceWidth } from '@utils/device';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.white,
    justifyContent: 'center',
    alignItems: 'center',
  },
  header: {
    flex: 0.1,
    width: deviceWidth,
    paddingHorizontal: 30,
    paddingTop: 10,
  },
  carousel: {
    flex: 0.7,
  },
  footer: {
    flex: 0.2,
    width: deviceWidth,
    paddingHorizontal: 30,
  },
  skipText: {
    ...fontSizes.fontMedium,
    color: color.primaryColor,
    textAlign: 'right',
    fontWeight: '600',
  },
  dot: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: 8,
    backgroundColor: color.primaryColor,
  },
  inactiveDot: {
    backgroundColor: color.secondaryColor,
  },
  textSignup: {
    ...fontSizes.fontMedium,
    color: color.gray,
    fontWeight: '600',
    textAlign: 'center',
    marginTop: 20,
  },
  textSignupRed: {
    color: color.primaryColor,
  },
});

export default styles;
